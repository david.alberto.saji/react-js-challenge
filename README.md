# Frontend Challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Scripts


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run storybook`

Launches Storybook at [http://localhost:6006](http://localhost:6006)