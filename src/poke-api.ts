import axios from 'axios';
import { baseURL } from './constants';

axios.defaults.adapter = require('axios/lib/adapters/http');

const api = axios.create({ baseURL });

export async function fetch(url: string) {
    const { data } = await api.get(url);
    return data;
}

export async function fetchPokemonList(page = 0, limit = 30) {
    const offset = (page - 1) * limit;
    const { data } = await api.get('pokemon/', { params: { limit, offset } });
    return data;
}

export async function fetchPokemonById(id: number) {
    const { data } = await api.get(`pokemon/${id}`);
    return data;
}
