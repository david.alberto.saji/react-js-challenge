export function getPokemonId(url: string) {
    const regex = /^https:\/\/pokeapi.co\/api\/v2\/pokemon\/(?<id>\d+)\/$/;
    const match = url.match(regex);
    return parseInt(match?.groups?.id ?? '1', 10);
}

export function getPokemonSprite(pokeId: number) {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokeId}.png`
}
