import { render, screen } from '@testing-library/react';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import nock from 'nock';
import Pokedex from './Pokedex';
import { baseURL, dummyPokedexData, dummySpeciesData } from '../../constants';

const queryClient = new QueryClient();

beforeEach(() => {
    nock(baseURL)
        .get('/pokemon/2')
        .reply(200, dummyPokedexData)
        .get('/pokemon-species/2/')
        .reply(200, dummySpeciesData);
});

test('renders Pokedex', async () => {
    render(
        <MemoryRouter initialEntries={['/pokedex/2']}>
            <QueryClientProvider client={queryClient}>
                <Routes>
                    <Route path="pokedex/:id" element={<Pokedex />} />
                </Routes>
            </QueryClientProvider>
        </MemoryRouter>
    );
    await screen.findByText(/Pokedex/i);
    await screen.findByText(/#2 ivysaur/i);
    screen.getByText(/grass/i);
    screen.getByText(/poison/i);
    screen.getByText(/HT: 1.0m/i);
    screen.getByText(/WT: 13.0kg/i);
    screen.getByAltText('sprite');
    await screen.findByText(/When the bulb on its back grows large, it appears to lose the ability to stand on its hind legs./i);
});
