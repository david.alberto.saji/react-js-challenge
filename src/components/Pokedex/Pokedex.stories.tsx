import { ComponentMeta } from '@storybook/react';
import Pokedex from './Pokedex';

export default {
    title: 'Pokedex',
    component: Pokedex,
} as ComponentMeta<typeof Pokedex>;

export const Bulbasaur = () => <Pokedex />;
