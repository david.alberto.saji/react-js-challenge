import { useParams, useNavigate } from 'react-router-dom';
import { useQuery } from 'react-query';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';
import Chip from '@mui/material/Chip';
import Divider from '@mui/material/Divider';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import { useReadLocalStorage } from 'usehooks-ts';
import { fetchPokemonById, fetch } from '../../poke-api';

export default function Pokedex() {
    const navigate = useNavigate();
    const params = useParams();
    const id = parseInt(params.id || '1', 10);
    const page = useReadLocalStorage<number>('page');
    const { data = {}, isLoading, isError, error } = useQuery<any, Error>(
        ['pokemon-info', id],
        () => fetchPokemonById(id),
    );
    const speciesUrl = data?.species?.url;
    const {
        data: speciesData,
        isError: isSpeciesError,
        error: speciesError,
    } = useQuery<any, Error>(
        ['pokemon-species', speciesUrl],
        () => fetch(speciesUrl),
        { enabled: !!speciesUrl }
    );
    if (isError) {
        return <div>Error: {error?.message}</div>
    }
    if (isSpeciesError) {
        return <div>Error: {speciesError?.message}</div>
    }
    const { name, height, weight, sprites, types } = data;
    let description: string | undefined;
    if (speciesData) {
        const flavorData = speciesData.flavor_text_entries.find(({ language }: any) => language.name === 'en');
        description = flavorData.flavor_text.replace(/\f/g, ' ');
    }
    const handleBackClick = () => navigate(`/poke-grid?page=${page || 1}`);
    return (
        <Box sx={{ mx: { xs: 0, md: 12 }, my: 2 }}>
            <Typography variant="h3" mb={3} textAlign="center">Pokedex</Typography>
            <Button onClick={handleBackClick} sx={{ ml: 3, mb: 2 }}>
                <KeyboardArrowLeft />
                Back
            </Button>
            <Box
                display="grid"
                gridTemplateColumns={{
                    xs: "auto",
                    md: "repeat(2, 200px) 1fr",
                }}
                gridTemplateRows="auto"
                rowGap={{
                    xs: 2,
                    md: 0,
                }}
                mx={10}
                maxWidth={800}
                gridTemplateAreas={{
                    xs: `
                        "sprite"
                        "name"
                        "type"
                        "divider"
                        "height"
                        "weight"
                        "description"
                    `,
                    md: `
                        "sprite sprite name"
                        "sprite sprite type"
                        "sprite sprite divider"
                        "sprite sprite height"
                        "sprite sprite weight"
                        "description description description"
                    `
                }}
            >
                {isLoading ? (
                    <>
                        <Skeleton sx={{ gridArea: 'sprite', m: "auto", width: 200, height: 200 }} animation="wave" />
                        <Skeleton sx={{ gridArea: 'name', height: 50 }} animation="wave" />
                        <Skeleton sx={{ gridArea: 'type', height: 50 }} animation="wave" />
                        <Box sx={{ gridArea: 'divider' }}><Divider /></Box>
                        <Skeleton sx={{ gridArea: 'height', height: 50 }} animation="wave" />
                        <Skeleton sx={{ gridArea: 'weight', height: 50 }} animation="wave" />
                        <Skeleton sx={{ gridArea: 'description', height: 50 }} animation="wave" />
                    </>
                ) : (
                    <>
                        <Box sx={{
                            gridArea: 'sprite',
                            m: "auto",
                            border: 2,
                            borderColor: 'grey.600',
                            borderRadius: 2,
                            bgcolor: 'grey.100',
                            width: 200,
                            height: 200
                        }}>
                            <img src={sprites.front_default} alt="sprite" width="100%" />
                        </Box>
                        <Box sx={{ gridArea: 'name' }}>
                            <Typography variant="h5" textTransform="capitalize">#{id} {name}</Typography>
                        </Box>
                        <Box sx={{ gridArea: 'type' }}>
                            <Stack direction="row" spacing={1}>
                                {types.map(({ type }: any) => (
                                    <Chip key={type.name} label={type.name} variant="outlined" sx={{ typography: 'overline' }} />
                                ))}
                            </Stack>
                        </Box>
                        <Box sx={{ gridArea: 'divider' }}>
                            <Divider />
                        </Box>
                        <Box sx={{ gridArea: 'height' }}>
                            <Typography variant="body1">HT: {(height * 0.1).toFixed(1)}m</Typography>
                        </Box>
                        <Box sx={{ gridArea: 'weight' }}>
                            <Typography variant="body1">WT: {(weight * 0.1).toFixed(1)}kg</Typography>
                        </Box>
                        <Grid sx={{ gridArea: 'description', pt: { xs: 0, md: 3 } }}>
                            {description ? (
                                <Typography
                                    variant="body1"
                                    sx={{
                                        border: 1,
                                        color: 'info.main',
                                        borderRadius: 2,
                                        p: 1,
                                        px: 2,
                                        overflowWrap: 'break-word',
                                    }}>
                                    {description}
                                </Typography>
                            ) : (
                                <Skeleton sx={{ gridArea: 'description', height: 50 }} />
                            )}
                        </Grid>
                    </>
                )}
            </Box>
        </Box>
    );
}