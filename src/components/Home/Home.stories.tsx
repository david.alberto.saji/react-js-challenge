import { ComponentStory, ComponentMeta } from '@storybook/react';
import Home from './Home';

export default {
  title: 'Home',
  component: Home,
} as ComponentMeta<typeof Home>;

export const LandingPage: ComponentStory<typeof Home> = () => <Home />;