import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Home from './Home';

const mockNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  useNavigate: () => mockNavigate,
}))

test('renders start button', async () => {
  render(<Home />);
  const button = screen.getByRole('button', { name: /^START$/ });
  userEvent.click(button);
  expect(mockNavigate).toHaveBeenCalledTimes(1);
  expect(mockNavigate).toHaveBeenCalledWith('/poke-grid');
});
