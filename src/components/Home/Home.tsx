import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

export default function Home() {
    const navigate = useNavigate();
    return (
        <Box sx={{ m: 2, textAlign: 'center' }}>
            <Typography variant="h3" mb={3}>Poke Challenge</Typography>
            <Button variant="contained" onClick={() => navigate('/poke-grid')}>START</Button>
        </Box>
    )
}