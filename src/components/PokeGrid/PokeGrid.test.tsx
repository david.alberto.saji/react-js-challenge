import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import nock from 'nock';
import PokeGrid from './PokeGrid';
import { baseURL, dummyPokeGridDataPage1, dummyPokeGridDataPage2 } from '../../constants';

const queryClient = new QueryClient();

const Wrapper = () => (
    <MemoryRouter initialEntries={['/poke-grid?page=1']}>
        <QueryClientProvider client={queryClient}>
            <Routes>
                <Route path="poke-grid" element={<PokeGrid />} />
            </Routes>
        </QueryClientProvider>
    </MemoryRouter>
);

beforeEach(() => {
    nock(baseURL)
        .get('/pokemon/')
        .query({ limit: 30, offset: 0 })
        .reply(200, dummyPokeGridDataPage1)
        .get('/pokemon/')
        .query({ limit: 30, offset: 30 })
        .reply(200, dummyPokeGridDataPage2);
});

test('renders PokeGrid page 1', async () => {
    const { container } = render(<Wrapper />);
    await screen.findByText(/bulbasaur/i);
    expect(container).toMatchSnapshot();
});

test('renders PokeGrid page 2', async () => {
    render(<Wrapper />);
    await screen.findByText(/bulbasaur/i);
    expect(screen.queryByText(/nidoqueen/i)).not.toBeInTheDocument();
    const page2 = screen.getByRole('button', { name: 'Go to page 2' });
    userEvent.click(page2);
    await screen.findByText(/nidoqueen/i);
    expect(screen.queryByText(/bulbasaur/i)).not.toBeInTheDocument();
});

test('filter favorites', async () => {
    render(<Wrapper />);
    await screen.findByText(/bulbasaur/i);
    const addToFavoritesBtns = screen.getAllByRole('button', { name: 'add to favorites' });
    const filterFavoritesBtn = screen.getByLabelText(/only favorites/i);

    userEvent.click(addToFavoritesBtns[0]);

    userEvent.click(filterFavoritesBtn);
    expect(screen.getByText(/bulbasaur/i)).toBeInTheDocument();
    expect(screen.queryByText(/ivysaur/i)).not.toBeInTheDocument();

    const removeFromFavoritesBtns = screen.getAllByRole('button', { name: 'remove from favorites' });
    userEvent.click(removeFromFavoritesBtns[0]);
    expect(screen.queryByText(/bulbasaur/i)).not.toBeInTheDocument();
});

test('filter by name', async () => {
    render(<Wrapper />);
    await screen.findByText(/bulbasaur/i);
    userEvent.type(screen.getByLabelText('Search name'), 'Charizard');
    expect(screen.queryByText(/bulbasaur/i)).not.toBeInTheDocument();
    expect(screen.getAllByText(/charizard/i)).toHaveLength(2);
});