import { ComponentMeta } from '@storybook/react';
import PokeGrid from './PokeGrid';

export default {
    title: 'PokeGrid',
    component: PokeGrid,
} as ComponentMeta<typeof PokeGrid>;

export const Page1 = () => <PokeGrid />;
