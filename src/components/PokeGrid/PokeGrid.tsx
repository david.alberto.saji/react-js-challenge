import React, { useState } from 'react';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Stack from '@mui/material/Stack';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import { useQuery } from 'react-query';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useLocalStorage } from 'usehooks-ts';
import { fetchPokemonList } from '../../poke-api';
import { getPokemonId } from '../../helpers';
import PokeCard from '../PokeCard';

const limit = 30;
const initialData = { count: 0, results: [] };

export default function PokeGrid() {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();
    let page = parseInt(searchParams.get('page') || '1', 10);
    page = isNaN(page) ? 1 : page;
    const [, setPage] = useLocalStorage('page', 1);
    const [favorites, setFavorites] = useLocalStorage<string[]>('favorites', []);
    const [onlyFavorites, setOnlyFavorites] = useState(false);
    const [searchValue, setSearchValue] = useState<string | null>(null);
    const [searchInputValue, setSearchInputValue] = useState('');
    const { data = initialData, isLoading, isError, error } = useQuery<any, Error>(
        ['pokemon', page],
        () => fetchPokemonList(page, limit)
    );
    const handlePageChange = (_evt: React.ChangeEvent<unknown>, page: number) => {
        setSearchParams({ page: page.toString() })
    }
    const onCardClick = (pokeId: number) => {
        setPage(page);
        navigate(`/pokedex/${pokeId}`);
    }
    const onAddToFavorites = (pokeName: string) => {
        setFavorites((favs) => ([...favs, pokeName]));
    }
    const onRemoveFromFavorites = (pokeName: string) => {
        setFavorites((favs) => favs.filter((fav: any) => fav !== pokeName));
    }
    if (isError) {
        return <div>Error: {error?.message}</div>
    }
    let { count, results: pokemonList } = data;
    const pages = Math.ceil(count / limit);
    if (searchInputValue) {
        pokemonList = pokemonList.filter((poke: any) => poke.name.toLowerCase().includes(searchInputValue.toLowerCase()));
    }
    if (onlyFavorites) {
        pokemonList = pokemonList.filter((poke: any) => favorites.includes(poke.name));
    }
    return (
        <Box sx={{ flexGrow: 1, m: 2 }}>
            <Typography variant="h3" textAlign="center">
                PokeGrid
            </Typography>
            <Stack direction={{ xs: "column", md: "row-reverse" }} spacing={2} alignItems="center">
                <Autocomplete
                    freeSolo
                    id="search-name"
                    sx={{ width: 200, p: 2 }}
                    options={pokemonList.map((option: any) => option.name)}
                    value={searchValue}
                    onChange={(_evt: any, newValue: string | null) => {
                        setSearchValue(newValue);
                    }}
                    inputValue={searchInputValue}
                    onInputChange={(_evt, newInputValue) => {
                        setSearchInputValue(newInputValue);
                    }}
                    renderInput={(params) => <TextField {...params} label="Search name" />}
                />
                <FormGroup>
                    <FormControlLabel
                        control={(
                            <Checkbox
                                checked={onlyFavorites}
                                onChange={(evt: React.ChangeEvent<HTMLInputElement>) => setOnlyFavorites(evt.target.checked)}
                                inputProps={{ 'aria-label': 'only favorites' }}
                            />
                        )}
                        label="Only favorites" />
                </FormGroup>
            </Stack>
            <Grid container spacing={2} p={2}>
                {isLoading
                    ? Array.from(Array(30)).map((_, index) => (
                        <Grid item xs={4} key={index}>
                            <Skeleton height={100} animation="wave" />
                        </Grid>
                    )) : pokemonList.map((pokemon: any) => {
                        const pokeId = getPokemonId(pokemon.url);
                        return (
                            <PokeCard
                                key={pokeId}
                                pokeId={pokeId}
                                pokeName={pokemon.name}
                                isFavorite={favorites.includes(pokemon.name)}
                                onClick={onCardClick}
                                onAddToFavorites={onAddToFavorites}
                                onRemoveFromFavorites={onRemoveFromFavorites}
                            />
                        );
                    })
                }
            </Grid>
            <Pagination count={pages} color="primary" sx={{ width: 400, mx: "auto", my: 2 }} page={page} onChange={handlePageChange} />
        </Box>
    );
}