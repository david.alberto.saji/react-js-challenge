import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import nock from 'nock';
import App from '../App';
import {
  baseURL,
  dummyPokeGridDataPage1,
  dummyPokedexData,
  dummySpeciesData
} from '../../constants';

beforeEach(() => {
  nock(baseURL)
      .get('/pokemon/')
      .query({ limit: 30, offset: 0 })
      .reply(200, dummyPokeGridDataPage1)
      .get('/pokemon/2')
      .reply(200, dummyPokedexData)
      .get('/pokemon-species/2/')
      .reply(200, dummySpeciesData);
});

test('go through the site', async () => {
  render(<App />);
  userEvent.click(screen.getByText('START'));
  await screen.findByText(/bulbasaur/i);
  expect(screen.getByText(/ivysaur/i)).toBeInTheDocument();
  userEvent.click(screen.getAllByRole('button', { name: 'card action area' })[1]);
  await screen.findByText(/#2 ivysaur/i);
  expect(screen.queryByText(/bulbasaur/i)).not.toBeInTheDocument();
  await screen.findByText(/When the bulb on its back grows large, it appears to lose the ability to stand on its hind legs./i);
  userEvent.click(screen.getByRole('button', { name: /back/i }));
  await screen.findByText(/bulbasaur/i);
});
