import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PokeCard from './PokeCard';

test('renders PokeCard', async () => {
    const props = {
        pokeId: 1,
        pokeName: 'bulbasaur',
        isFavorite: false,
        onClick: jest.fn(),
        onAddToFavorites: jest.fn(),
        onRemoveFromFavorites: jest.fn(),
    }
    render(<PokeCard {...props} />);
    expect(screen.getByText(/bulbasaur/i)).toBeInTheDocument();
    const cardActionArea = screen.getByRole('button', { name: 'card action area' });
    userEvent.click(cardActionArea);
    expect(props.onClick).toHaveBeenCalledTimes(1);
    expect(props.onClick).toHaveBeenCalledWith(1);
});

test('add to favorites', async () => {
    const props = {
        pokeId: 1,
        pokeName: 'bulbasaur',
        isFavorite: false,
        onClick: jest.fn(),
        onAddToFavorites: jest.fn(),
        onRemoveFromFavorites: jest.fn(),
    }
    render(<PokeCard {...props} />);
    const addToFavoritesBtn = screen.getByRole('button', { name: 'add to favorites' });
    userEvent.click(addToFavoritesBtn);
    expect(props.onAddToFavorites).toHaveBeenCalledTimes(1);
    expect(props.onAddToFavorites).toHaveBeenCalledWith('bulbasaur');
});

test('remove from favorites', async () => {
    const props = {
        pokeId: 1,
        pokeName: 'bulbasaur',
        isFavorite: true,
        onClick: jest.fn(),
        onAddToFavorites: jest.fn(),
        onRemoveFromFavorites: jest.fn(),
    }
    render(<PokeCard {...props} />);
    const removeFromFavoritesBtn = screen.getByRole('button', { name: 'remove from favorites' });
    userEvent.click(removeFromFavoritesBtn);
    expect(props.onRemoveFromFavorites).toHaveBeenCalledTimes(1);
    expect(props.onRemoveFromFavorites).toHaveBeenCalledWith('bulbasaur');
});