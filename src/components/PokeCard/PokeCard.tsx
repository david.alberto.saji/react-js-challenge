import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import CardActionArea from '@mui/material/CardActionArea'
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import { getPokemonSprite } from '../../helpers'

interface PokeCardProps {
    pokeId: number;
    pokeName: string;
    isFavorite: boolean;
    onClick: (pokeId: number) => void;
    onAddToFavorites: (pokeName: string) => void;
    onRemoveFromFavorites: (pokeName: string) => void;
}

export default function PokeCard({
    pokeId,
    pokeName,
    isFavorite,
    onClick,
    onAddToFavorites,
    onRemoveFromFavorites,
}: PokeCardProps) {
    return (
        <Grid item xs={4}>
            <Card sx={{ bgcolor: 'grey.100' }}>
                <CardActions sx={{ pb: 0 }}>
                    {isFavorite ? (
                            <IconButton aria-label="remove from favorites" onClick={() => onRemoveFromFavorites(pokeName)}>
                                <FavoriteIcon />
                            </IconButton>
                        ) : (
                            <IconButton aria-label="add to favorites" onClick={() => onAddToFavorites(pokeName)}>
                                <FavoriteBorderIcon />
                            </IconButton>
                        )
                    }
                </CardActions>
                <CardActionArea onClick={() => onClick(pokeId)} aria-label="card action area">
                    <CardMedia
                        component="img"
                        sx={{ width: 120, mx: 'auto' }}
                        image={getPokemonSprite(pokeId)}
                        alt={`${pokeName} sprite `}
                    />
                    <CardContent sx={{ p: 0, pb: 1, m: 0 }}>
                        <Typography variant="h5" color="text.secondary" textTransform="capitalize" textAlign="center">
                            {pokeName}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    );
}