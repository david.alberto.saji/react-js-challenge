import { ComponentStory, ComponentMeta } from '@storybook/react';
import PokeCard from './PokeCard';

export default {
    title: 'PokeCard',
    component: PokeCard,
} as ComponentMeta<typeof PokeCard>;

const Template: ComponentStory<typeof PokeCard> = (args) => <PokeCard {...args} />;

export const Bulbasaur = Template.bind({});

Bulbasaur.args = {
    pokeId: 1,
    pokeName: 'bulbasaur',
    isFavorite: false,
    onClick: (_pokeId: number) => {},
    onAddToFavorites: (_pokeName: string) => {},
    onRemoveFromFavorites: (_pokeName: string) => {},
};