import { getPokemonId, getPokemonSprite } from './helpers';

test('get pokemon id', async () => {
    expect(getPokemonId('https://pokeapi.co/api/v2/pokemon/2/')).toBe(2);
});

test('get pokemon sprite', async () => {
    expect(getPokemonSprite(2)).toBe('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png');
});
